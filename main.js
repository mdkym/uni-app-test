import Vue from 'vue'
import App from './App'
import { myRequest } from './util/api.js'

//引入uView前端UI框架
import uView from './uni_modules/uview-ui'
Vue.use(uView)

Vue.prototype.$myRequest = myRequest

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
